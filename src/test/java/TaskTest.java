/**
 * @package PACKAGE_NAME
 * @Class TaskTest
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午11:38
 */

import com.github.zxhtom.crontab.CrontabApplication;
import com.github.zxhtom.crontab.config.v2.CronTaskRegistrar;
import com.github.zxhtom.crontab.config.v2.SchedulingRunnable;
import com.github.zxhtom.crontab.mapper.TestMapper;
import com.github.zxhtom.crontab.schedule.DemoTask;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @program: simple-demo
 * @description: 测试定时任务
 * @author: CaoTing
 * @date: 2019/5/23
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CrontabApplication.class)
public class TaskTest {

    @Autowired
    CronTaskRegistrar cronTaskRegistrar;

    @Test
    public void testTask() throws InterruptedException {
        SchedulingRunnable task = new SchedulingRunnable(TestMapper.class, "getTests", null);
        SchedulingRunnable task2 = new SchedulingRunnable(TestMapper.class, "getTests", null);
        cronTaskRegistrar.addCronTask(task, "0/10 * * * * ?");
        // 便于观察
        Thread.sleep(20000);
        cronTaskRegistrar.addCronTask(task2, "0/1 * * * * ?");
        Thread.sleep(2000000000);
    }

    @Test
    public void testHaveParamsTask() throws InterruptedException {
        SchedulingRunnable task = new SchedulingRunnable(DemoTask.class, "taskWithParams", "haha", 23);
        cronTaskRegistrar.addCronTask(task, "0/10 * * * * ?");

        // 便于观察
        Thread.sleep(3000000);
    }
}
