package com.github.zxhtom.crontab.config.v2;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.config.CronTask;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @package com.github.zxhtom.crontab.config.v2
 * @Class CronTaskRegistrar
 * @Description 添加定时任务注册类、增加、删除定时任务
 * @Author zhangxinhua
 * @Date 19-11-15 上午11:05
 * 继承DisposableBean表示在Bean被销毁的时候，spring会调用destory方法回调。
 */
@Component
public class CronTaskRegistrar implements DisposableBean {

    @Autowired
    TaskScheduler taskScheduler;

    private final Map<Runnable, ScheduledTask> scheduledTaskMap = new ConcurrentHashMap<>();

    public TaskScheduler getTaskScheduler() {
        return this.taskScheduler;
    }

    public void addCronTask(Runnable task, String cron) {
        addCronTask(new CronTask(task,cron));
    }

    public void addCronTask(CronTask cronTask) {
        if (cronTask != null) {
            Runnable runnable = cronTask.getRunnable();
            if (this.scheduledTaskMap.containsKey(runnable)) {
                removeCronTask(runnable);
            }
            //添加
            this.scheduledTaskMap.put(runnable, scheduleCronTask(cronTask));
        }
    }

    /**
     * 移除定时任务
     * @param task
     */
    public void removeCronTask(Runnable task) {
        ScheduledTask scheduledTask = this.scheduledTaskMap.remove(task);
        if (scheduledTask != null)
            scheduledTask.cancel();
    }
    public ScheduledTask scheduleCronTask(CronTask cronTask) {
        ScheduledTask scheduledTask;
        scheduledTask = new ScheduledTask();
        scheduledTask.future = this.taskScheduler.schedule(cronTask.getRunnable(), cronTask.getTrigger());
        return scheduledTask;
    }
    @Override
    public void destroy() throws Exception {
        for (ScheduledTask task : this.scheduledTaskMap.values()) {
            task.cancel();
        }
        this.scheduledTaskMap.clear();
    }
}
