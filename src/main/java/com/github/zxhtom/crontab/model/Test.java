package com.github.zxhtom.crontab.model;

import lombok.Data;

/**
 * @package com.github.zxhtom.crontab.model
 * @Class Test
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午9:39
 */
@Data
public class Test {
    private int id;
    private String test;
}
