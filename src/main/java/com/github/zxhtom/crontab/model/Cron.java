package com.github.zxhtom.crontab.model;

import lombok.Data;

/**
 * @package com.github.zxhtom.crontab.model
 * @Class Cron
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午10:20
 */
@Data
public class Cron {
    private int id;
    private String cron;
}
